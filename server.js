var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());
//bodyParser esta dentro de express por lo que no hace falta instalarlo poniendo
//estoo y sin poner nada en la funcion parsea el body en formato json
//es importante que bodyparser vaya despues de express
app.listen(port);

console.log("API nueva escuchando en el puerto " + port);

app.get("/apitechu/v1",
          function(req, res) {
            console.log("GET /apitechu/v1 ");
            res.send({"msg":"Hola desde API Techu"});
              }

);



app.get("/apitechu/v1/users",
        function(req, res) {
         console.log("GET /apitechu/v1/users");
  //       res.sendFile('./usuario.json'); DEPRECATED
             res.sendFile('usuarios.json', {root: __dirname});
  //         var users = require('./usuarios.json');
  //         res.send(users);
        }
);



app.post("/apitechu/v1/users",
        function(req,res) {
           console.log("POST /apitechu/v1/users");
//el metodo post tiene dos variable, la url y la funcion manejadora
//de entrada , salida
//           console.log(req.headers);
           console.log("first_name is " + req.body.first_name);
           console.log("last_name is " + req.body.last_name);
           console.log("pais is  " + req.body.pais);


      var newUser = {
        "first_name" : req.body.first_name,
        "last_name"  : req.body.last_name,
        "pais"    : req.body.pais
      };
      // defino una nueva variable con los datos de la cabecera
      var users = require('./usuarios.json');
      //internamente se cargan los arrays del fichero en la variable users
      //y la variable users pasa a ser de tipo array

      users.push(newUser);
      //la funcion push añade un nuevo registro a la variable user,
      // la info la coge de la variable newUser
      writeUserDataToFile(users);
      console.log("Usuario guardado con éxito");
      res.send({"msg": "Usuario guardado con éxito"});
    }
  );




//verbo delete
app.delete("/apitechu/v1/users/:id",
  function(req,res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log(req.params);
  console.log(req.params.id);

  var users = require('./usuarios.json');
  users.splice(req.params.id-1,1);
//funcion cortar, a partir del registro id-1 , corto 1 elemento
  writeUserDataToFile(users);
  console.log("Usuario borrado");
   res.send({"msg" : "Usuario borrado"});
   }
);




function writeUserDataToFile(data) {
  var fs = require('fs');
  //para hacer esto me he instalado previamente fs en la consola
  //cargo la funcionalidad de fs
  var jsonUserData = JSON.stringify(data);
  //si no hago stringify en vez de guardalo en formato json se guardaria
  //como un objeto js
  fs.writeFile("./usuarios.json",jsonUserData, "utf8",
  function(err) {
    if(err) {
//      var msg = "Error al escribir fichero usuarios";
//      console.log(msg);
         console.log(err);
       }  else{
//      var msg = "Usuario persistido";
//      console.log(msg);
         console.log("Datos escritos en archivo");
      }

     }
   );

 //en la funcion writefile meto como parametros,
 // el fichero de salida, los datos a meter el fichero
 //el formato de encoding y una funcion de error que me diga
 //si ha ido bien o mal
//    res.send(users);
//devuelvo la variable users que contiene la info del fichero mas
//un registro

//      console.log("Usuario añadido con exito ");

};


app.post("/apitechu/v1/monstruo/:p1/:p2",
   function(req,res)   {
   console.log("Parámetros");
   console.log(req.params);
//aparece en la consola lo que hemos metido como p1 y p2

   console.log("Query String");
   console.log(req.query);
//aparece en la consola lo que hemos metido como  ?



   console.log("Body");
   console.log(req.body);
//

   console.log("Headers");
   console.log(req.headers);
   }
//aparece en la consola los headers definidos por nosotros y los
//de base
);


app.post("/apitechu/v1/login",
   function(req,res)   {
    console.log("/apitechu/v1/login");
//   console.log("Parámetros");
//   console.log(req.params);
//aparece en la consola lo que hemos metido como a1 y a2

//   console.log("Body");
//   console.log(req.body);

   console.log("my email is " + req.body.email);
   console.log("my password  is " + req.body.password);





// defino una nueva variable con los datos de la cabecera
  //    console.log("antes de allusers");
         var allusers = require('./userpassword.json');
  //     console.log("despues de alluers");

          flag = 0;
         for (user of allusers) {
           if ( req.body.email == user.email &&
               req.body.password == user.password )
             {
                console.log("login correcto");
                console.log("id usuario " + user.id);


             user.logged=true;
             writeUserDataToFile2(allusers);
             flag = 1;
             res.send({"msg" : "login correcto", "id usuario" : user.id});
    //          res.send({}  );


           }
              };
  //            console.log(flag);
          if (flag == 0)
            {
         console.log("login incorrecto");
         res.send({"msg" : "login incorrecto"});

           }
       }

);

app.post("/apitechu/v1/logout",
   function(req,res)   {
  console.log("/apitechu/v1/logout")
//   console.log("Parámetros");
//   console.log(req.params);
//aparece en la consola lo que hemos metido como a1 y a2

//   console.log("Body");
//   console.log(req.body);

   console.log("my id is " + req.body.id);






// defino una nueva variable con los datos de la cabecera
         var allusers = require('./userpassword.json');
//         console.log(allusers);
//aparece en la consola los headers definidos por nosotros y los
//de base
           flag = 0;
         for (user of allusers) {
           if ( user.id == req.body.id &&
                         user.logged == true )
                    {
               console.log("logout correcto");
                console.log("id usuario" + user.id);

        //       else {
          //      console.log(JSON.stringify(user.email));
            //      console.log(JSON.stringify(user.password));
  //             }
             delete user.logged;
           writeUserDataToFile2(allusers);
            flag = 1;
            res.send({"msg" : "logout correcto",
                    "id usuario" : user.id});
                    }
           };

           if (flag == 0)
             {
          console.log("logout incorrecto");
          res.send({"msg" : "logout incorrecto"});
            }

}

);

function writeUserDataToFile2(data) {
  var fs = require('fs');
  //para hacer esto me he instalado previamente fs en la consola
  //cargo la funcionalidad de fs
  var jsonUserData = JSON.stringify(data);
  //si no hago stringify en vez de guardalo en formato json se guardaria
  //como un objeto js
  fs.writeFile("./userpassword.json",jsonUserData, "utf8",
  function(err) {
    if(err) {
//      var msg = "Error al escribir fichero usuarios";
//      console.log(msg);
         console.log(err);
       }  else{
//      var msg = "Usuario persistido";
//      console.log(msg);
         console.log("Datos escritos en archivo usuarios");
      }

     }
   );

 //en la funcion writefile meto como parametros,
 // el fichero de salida, los datos a meter el fichero
 //el formato de encoding y una funcion de error que me diga
 //si ha ido bien o mal
//    res.send(users);
//devuelvo la variable users que contiene la info del fichero mas
//un registro

//      console.log("Usuario añadido con exito ");

};
